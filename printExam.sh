#!/bin/bash
# Script-Name: printexam
<<<<<<< HEAD
# Version    : 0.2
# Autor      : M.Spoerl
# Datum      : 15.01.2020
=======
# Version    : 0.1
# Autor      : Marks
# Datum      : 18.06.2019
>>>>>>> d243f408dd258d6dd99a7b7afafbb5d3766ac260
# Lizenz     : ...
# ... druckt eine Angabe und eine Loesung von Lyx Dateien auf Basis der Exam-Klasse
# ... zudem wird eine Loesung mit Bepuntkung gedruckt. Hierzu wird im Latex Dokument eine Kommentar {sol} bentutzt.

exportLyx(){
	
	mkdirPrint
	local dateiName=$1
	local lyxName=$2
	printDir="print/"
	lyxDatei_buff=$lyxName"_buff.lyx"
	errorLog=$printDir"lyxLog.txt"
	pdfDatei_angabe=$printDir$lyxName"_angabe.pdf"
	pdfDatei_loesung=$printDir$lyxName"_loesung.pdf"
	pdfDatei_loesungPunkte=$printDir$lyxName"_loesung_bepunktung.pdf"

	exportSkipStatus=0
	lyxErrorStatus=0
	
	# answer Parameter wird entfernt
	sed 's/^\(\\options \)\(.*\)\(, *answers\)\(.*\)$/\1\2\4/' "$lyxDatei" > "$lyxDatei_buff"
	# Lyx export
	lyxExport $pdfDatei_angabe

	# answers Parameter setzen (wird bei vorhandenem "answers" nochmal gesetzt!)
	# Zudem wird die Bepuntkunkt ausgeblendet. "-e" wird genutzt, um mehrere sed kommandos auszuführen
	sed -e 's/^\(\\options \)\(.*\)$/\1\2, answers/' -e 's/\(\includecomment{sol}\)/\excludecomment{sol}/1' "$lyxDatei" > "$lyxDatei_buff"
	# Lyx export
	lyxExport $pdfDatei_loesung
	
	# Details zur Bepunktung drucken 	
	sed -e 's/^\(\\options \)\(.*\)$/\1\2, answers/' -e 's/\(\excludecomment{sol}\)/\includecomment{sol}/1' "$lyxDatei" > "$lyxDatei_buff"
	# Lyx export	
	lyxExport $pdfDatei_loesungPunkte

	# Konsolenreport erstellen und Error Log löschen, wenn Programmablauf problemlos
	report
	
	# Bufferdatei loeschen
	rm "$lyxDatei_buff"
}

mkdirPrint(){
	if [[ ! -e print ]];
		then
		mkdir -p print
	fi
}

lyxExport(){
	# @Param: Outputdatei
	if [ 1 -eq $forceOverride ] ; then
		eval lyx -batch --export-to pdf "$1 $lyxDatei_buff" >> "$errorLog"  2>&1
	elif [ -e $1 ] ; then 
		read -p "Datei $1 überschreiben? (n/j/-f)" kommando;
		case "$kommando" in
			J|j|"") eval lyx -batch --export-to pdf "$1" "$lyxDatei_buff" >> "$errorLog"  2>&1
					lyxErrorStatus+=$?
			;;
			-F|-f) 	forceOverride=1
					eval lyx -batch --export-to pdf "$1" "$lyxDatei_buff" >> "$errorLog"  2>&1
					lyxErrorStatus+=$? 
			;;
			N|n)	echo -e "Datei $1 wurde \033[1mnicht\033[0m erstellt." 
					exportSkipStatus+=1
			;;
			*) 		echo "Unbekannter Parameter"	
					lyxExport $1
			;;
		esac
	else	#TODO: pruefen, ob erwuenscht
		eval lyx -batch --export-to pdf "$1" "$lyxDatei_buff" >> "$errorLog"  2>&1
	fi
}

report(){
	if [ 0 -eq $lyxErrorStatus ] ; then 
		if [ 0 -eq $exportSkipStatus ] ; then
			echo -e "Für \033[1m$lyxDatei\033[0m wurden \033[1mAngabe und Loesung\033[0m im PDF Format gedruckt."
			rm "$errorLog"
		elif [ 1 -eq $exportSkipStatus ] ; then
			echo -e "\033[1mNur\033[0m die gewünscht Datei wurde gedruckt."
			rm "$errorLog"
		else 
			echo -e "Es wurde \033[1mkeine\033[0m Datei gedruckt."
		fi
	else
		echo -e "!!!\n!!!\n!!!\nFehler beim Drucken der Datei $lyxDatei \n!!!\nDetails sind unter $errorLog einsehbar.\n!!!\n!!!\n!!!"
	fi
}

exportLatest(){
	local dateiName=$1
	local lyxName=$2
	local bold=$(tput bold)
	local normal=$(tput sgr0)
		read -p "...soll Datei ${bold}$dateiName${normal} gedruckt werden? (n/j/-f)" kommando;
		case "$kommando" in
			J|j|"") exportLyx $1 $2
			;; 
			N|n)	echo -e "Datei $dateiName wurde \033[1mnicht\033[0m erstellt." 
					exportSkipStatus+=1
			;;
			-F|-f) 	forceOverride=1
					exportLyx $1 $2
			;;
			*) 		echo "Unbekannter Parameter"	
					#takeLatest $dateiName
			;;
		esac
}

if [ "-f" == $2 ] ; then
	forceOverride=1
else
	forceOverride=0
fi

if [ -z $1 ] 
	then
	echo "Keine Datei gewählt..."
	# Dateien werden nach Datum sortiert, neueste Lyxdatei wird ausgewählt
	lyxDatei=$(ls -tr | grep .lyx$ | tail -n 1)
	lyxName="${lyxDatei%.lyx}"
	exportLatest $lyxDatei $lyxName

elif [ -e $1.lyx ] 
	then
	echo "-> Druck von $1.lyx wird gestartet."
	lyxDatei=$1.lyx
	lyxName="${lyxDatei%.lyx}"
	exportLyx $lyxDatei $lyxName
elif [ -e $1 ] 
	then
	if [ ${1##*.}=="lyx" ] 
		then
		echo "-> Druck von $1.lyx wird gestartet."
		extension="${1##*.}"
		lyxDatei=$1
		lyxName="${lyxDatei%.lyx}"
		exportLyx $lyxDatei $lyxName
	else
		echo "Datei $1 ist keine Lyx Datei"
	fi
else
	echo "Datei $1 exitiert nicht"
fi

exit 0

